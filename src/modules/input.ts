const REGEX_LETTERS = "[a-ząćęłńóśźż]{1}";
const REGEX_POSITIONS = "[01234?]{1}";

export function onLetterInput(event: Event) {
  let target = event.currentTarget as HTMLInputElement;

  if (!new RegExp(`${REGEX_LETTERS}`, "g").test(target.value)) {
    target.value = "";
  }
}

export function onPositionInput(event: Event) {
  let target = event.currentTarget as HTMLInputElement;

  if (!new RegExp(`${REGEX_POSITIONS}`, "g").test(target.value)) {
    target.value = "";
  }

  let correspondingLetterInput = document.querySelector(
    `[data-type="letter"][data-index="${target.dataset.index}"]`,
  ) as HTMLInputElement;
  if (correspondingLetterInput.value === "") {
    target.value = "";
  }
}
