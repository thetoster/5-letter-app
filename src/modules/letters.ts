export const ALPHABET_STR = "aąbcćdeęfghijklłmnńoópqrsśtuvwxyzźż";

export function generateLetters(): HTMLButtonElement[] {
  let buttons: HTMLButtonElement[] = [];
  ALPHABET_STR.split("").forEach((letter) => {
    let button = document.createElement("button");
    button.type = "button";
    button.className = "letter";
    button.innerHTML = letter;
    buttons.push(button);
  });
  return buttons;
}

export function onLetterClick(event: Event) {
  let target = event.currentTarget as HTMLButtonElement;
  target.classList.toggle("inactive");
}
