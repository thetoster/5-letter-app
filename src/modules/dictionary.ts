import { ALPHABET_STR } from "./letters";

const DICTIONARY_URL =
  "https://raw.githubusercontent.com/LibreOffice/dictionaries/master/pl_PL/pl_PL.dic";

const DICTIONARY_LOCAL_KEY = "dictionary";

/**
 * Returns dictionary containing polish 5-letter words
 */
export async function getDictionary(): Promise<string[] | null> {
  try {
    const response = await fetch(DICTIONARY_URL);
    const text = await response.text();
    const dictionaryString = decodeURIComponent(text);
    const dictionary = dictionaryString.split("\n");
    const filteredDictionary = dictionary.filter((word) =>
      new RegExp(`^[${ALPHABET_STR}]{5}$`, "gui").test(word),
    );
    return filteredDictionary;
  } catch (error) {
    console.error("There was an error while downloading dictionary", error);
    return null;
  }
}

/**
 * Refreshes dictionary
 */
export async function refreshDictionary() {
  const words = await getDictionary();
  localStorage.setItem(DICTIONARY_LOCAL_KEY, JSON.stringify(words));
}
