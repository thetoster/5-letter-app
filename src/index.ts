import { generateLetters, onLetterClick } from "./modules/letters";
import { onLetterInput, onPositionInput } from "./modules/input";
import { resetInputs } from "./modules/reset";
import { refreshDictionary } from "./modules/dictionary";
import { guessWord } from "./modules/guessing";

/**
 * Sets app the whole app when page loads
 */
function onLoad() {
  document
    .querySelectorAll('[data-type="letter"]')
    .forEach((el) => el.addEventListener("input", onLetterInput));

  document
    .querySelectorAll('[data-type="position"]')
    .forEach((el) => el.addEventListener("input", onPositionInput));

  document
    .querySelector(".btn-restart")
    ?.addEventListener("click", resetInputs);
  document
    .querySelector(".btn-refresh-dictionary")
    ?.addEventListener("click", refreshDictionary);

  document.querySelector(".used-letters")?.append(...generateLetters());
  document
    .querySelectorAll(".used-letters button")
    .forEach((el) => el.addEventListener("click", onLetterClick));

  document.getElementById("submit")?.addEventListener("click", guessWord);
}
window.addEventListener("load", onLoad);
