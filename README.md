# 5 Word App

App for generating 5-letter words to game called [Literalnie](https://literalnie.fun/). It will get all 5-letter words and generate array of words that will fit into requirements.

E.g. we have 4 known letters out of 5 and we know position of one of them. (R - first letter; A, I, N have unknown position). Get words that will start with R and will contain all of known letters.

If no letters, it will get random 5 letter word.

## How it's gonna work

We will see input, to enter know letters and its' positions. If positions is not known, place `?` there.

All of the inputs will have proper formatting in the UI.

*UI was inspired by <https://krzyzowki123.pl/>*

## Why?

First of all, I wanted to write the app to solve 5 letter puzzle, because I am lazy. Also I want to see what it's like to write app with typescript for electron before trying the same app in flutter.
Secondly, I wanted to learn typescript, to write better code. Code that is WAAAAYYYY easier to debug than plain JS.

*Edit: concept has changed*
I didn't think that electron itself cannot make a webapp... Therefore, I will stick to TS (cos that was one of the most important goals) and SASS, because I know it from other project.

## Requirements

- typescript installed globally
- sass installed globally
